# Revolut Task

![Example of Task](example.jpg)

### Created by Shaun Stone - 30/09/2017

This is a task carried out by **Shaun Michael Stone** using Preact, Redux, Sass, Webpack. I chose Preact because it's a lightweight equivalent to React, Redux as the favoured state container and Sass due to preference.

Base refers to the **FROM** Balance, Amount & Currency
Counter refers to the **TO** Balance, Amount & Currency

## Installation

**1. Clone this repo:**

```sh
git clone git@bitbucket.org:smks/revolut-task.git
cd revolut-task
```

**2. Install the dependencies:**

```sh
npm install
```

**3. Generate a production build in `./build`:**

```sh
npm run build
```

**4. Start local production server with [serve](https://github.com/zeit/serve):**

```sh
npm start
```

In your browser open up one of the URLs provided by the terminal. 

**Example below:**

```sh
Serving!
- Local: http://localhost:5000
- On Your Network:  http://192.168.1.68:5000
Copied local address to clipboard!
```
> This is to simulate a production (CDN) server with gzip. It just serves up the contents of `./build`.

To view the task, click **Exchange Money** button once the app has booted up.