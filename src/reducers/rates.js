import { FETCH_EXCHANGE_RATES } from '../actions';
import currencyCodes from '../constants/currency-codes';
import fx from 'money';

const INITIAL_STATE = {
  "USD": 1.3389,
  "EUR": 1.1341
};

// Setting the intitial values for the money library
fx.base = currencyCodes.GBP;
fx.rates = INITIAL_STATE;

export default function(state = INITIAL_STATE, action) {

  switch (action.type) {

  case FETCH_EXCHANGE_RATES: {
    const rates = action.payload.data.rates;
    fx.rates = rates; // required by money library
    return rates;
  }
  
  }

  return state;
}