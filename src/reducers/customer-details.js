import currencyCodes from './../constants/currency-codes';
import { UPDATE_BALANCES } from '../actions';

const INITIAL_STATE = {
  name: 'Tester'
};

INITIAL_STATE.balances = {};
INITIAL_STATE.balances[currencyCodes.GBP] = 650.00;
INITIAL_STATE.balances[currencyCodes.EUR] = 0.00;
INITIAL_STATE.balances[currencyCodes.USD] = 125.00;

/**
 * 
 * @param {*} state 
 * @param {*} action 
 */
export default function(state = INITIAL_STATE, action) {

  switch (action.type) {

    case UPDATE_BALANCES:
      let data = {};
      const { baseBalance, counterBalance, baseCurrencyCode, counterCurrencyCode } = action.payload.data;
      data.balances = INITIAL_STATE.balances;
      data.balances[baseCurrencyCode] = baseBalance;
      data.balances[counterCurrencyCode] = counterBalance;
      return { ...state, ...data };

  }

  return state;
}