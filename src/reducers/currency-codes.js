import currencyCodes from './../constants/currency-codes';

export default function() {
  return Object.keys(currencyCodes).map(k => currencyCodes[k]);
}