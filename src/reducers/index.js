import { combineReducers } from 'redux';

// import reducers
import CustomerDetailsReducer from './customer-details';
import AmountsReducer from './amounts';
import CurrencyCodesReducer from './currency-codes';
import RatesReducer from './rates';

const rootReducer = combineReducers({
  customerDetails: CustomerDetailsReducer,
  rates: RatesReducer,
  amounts: AmountsReducer,
  currencyCodes: CurrencyCodesReducer
});

export default rootReducer;