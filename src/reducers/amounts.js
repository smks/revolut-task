import currencyCodes from './../constants/currency-codes';
import {
  UPDATE_BASE_CURRENCY_CODE,
  UPDATE_COUNTER_CURRENCY_CODE,
  UPDATE_BASE_AMOUNT,
  UPDATE_COUNTER_AMOUNT
} from '../actions';
import fx from 'money';
import accounting from 'accounting';

const INITIAL_STATE = {
  baseCurrencyCode: currencyCodes.GBP,
  baseAmount: 1,
  baseRate: 1,
  counterCurrencyCode: currencyCodes.EUR,
  counterAmount: 1.13,
  counterRate: 1.13
};

export default function(state = INITIAL_STATE, action) {

  let data;

  switch (action.type) {

  case UPDATE_BASE_CURRENCY_CODE:
    data = {
      baseCurrencyCode: action.payload.currencyCode,
      counterRate: accounting.toFixed(fx.convert(1, {from: action.payload.currencyCode, to: state.counterCurrencyCode}), 2),
      counterAmount: accounting.toFixed(fx.convert(state.baseAmount, {from: action.payload.currencyCode, to: state.counterCurrencyCode}), 2)
    };
    return { ...state, ...data };

  case UPDATE_COUNTER_CURRENCY_CODE:
    data = {
      counterCurrencyCode: action.payload.currencyCode,
      counterRate: accounting.toFixed(fx.convert(1, {from: state.baseCurrencyCode, to: action.payload.currencyCode}), 2),
      counterAmount: accounting.toFixed(fx.convert(state.baseAmount, {from: state.baseCurrencyCode, to: action.payload.currencyCode}), 2)
    };
    return { ...state, ...data };

  case UPDATE_BASE_AMOUNT:
    data = {
      baseAmount: action.payload.amount,
      counterAmount: accounting.toFixed(fx.convert(action.payload.amount, {from: state.baseCurrencyCode, to: state.counterCurrencyCode}), 2)
    };
    return { ...state, ...data };

  case UPDATE_COUNTER_AMOUNT:
    data = {
      counterAmount: action.payload.amount,
      baseAmount: accounting.toFixed(fx.convert(action.payload.amount, {from: state.counterCurrencyCode, to: state.baseCurrencyCode}), 2)
    };
    return { ...state, ...data };

  }

  return state;
}