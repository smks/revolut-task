import currencyCodes from './../constants/currency-codes';
import axios from 'axios';

// API
const EXCHANGE_RATES_API_ROOT_URL = `http://api.fixer.io/latest`;

// Action Keys
export const FETCH_EXCHANGE_RATES = 'FETCH_EXCHANGE_RATES';
export const UPDATE_BASE_CURRENCY_CODE = 'UPDATE_BASE_CURRENCY_CODE';
export const UPDATE_COUNTER_CURRENCY_CODE = 'UPDATE_COUNTER_CURRENCY_CODE';
export const UPDATE_BASE_AMOUNT = 'UPDATE_BASE_AMOUNT';
export const UPDATE_COUNTER_AMOUNT = 'UPDATE_COUNTER_AMOUNT';
export const UPDATE_BALANCES = 'UPDATE_BALANCES';

/**
 * Makes call to http://fixer.io/
 * Uses redux-promise to resolve data
 * 
 * @param {*} baseCurrencyCode 
 */
export function fetchExchangeRates(baseCurrencyCode) {
  if (!baseCurrencyCode) {
    baseCurrencyCode = currencyCodes.GBP;
  }
  const url = `${EXCHANGE_RATES_API_ROOT_URL}?base=${baseCurrencyCode}&`;

  const request = axios.get(url, {
    params: {
      base: baseCurrencyCode,
      symbols: [
        currencyCodes.GBP,
        currencyCodes.EUR,
        currencyCodes.USD
      ].join(',')
    }
  });

  //const request = new Promise((res, rej) => res({data: {"base":"GBP","date":"2017-09-28","rates":{"USD":1.3389,"EUR":1.1341}}}));

  return {
    type: FETCH_EXCHANGE_RATES,
    payload: request
  };
}

// Changing the base currency updates the amounts
export function updateBaseCurrencyCode(currencyCode) {
  return {
    type: UPDATE_BASE_CURRENCY_CODE,
    payload: { currencyCode }
  };
}

// Changing the counter currency updates the amounts
export function updateCounterCurrencyCode(currencyCode) {
  return {
    type: UPDATE_COUNTER_CURRENCY_CODE,
    payload: { currencyCode }
  };
}

// Update the amount once user has entered base amount
export function updateBaseAmount(amount) {
  return {
    type: UPDATE_BASE_AMOUNT,
    payload: { amount }
  };
}

// Update the amount once user has entered counter amount
export function updateCounterAmount(amount) {
  return {
    type: UPDATE_COUNTER_AMOUNT,
    payload: { amount }
  };
}

// Balances to be updated by debiting base 
// balance and crediting counter balance if enough funds exist
export function updateBalances({
  baseCurrencyCode, 
  baseAmount, 
  counterCurrencyCode, 
  counterAmount,
  balances }) {

    let errors = [];
    let data = {
      baseBalance: balances[baseCurrencyCode],
      counterBalance: balances[counterCurrencyCode],
      baseCurrencyCode,
      counterCurrencyCode
    };

    // don't try and exchange the same currency balance
    if (baseCurrencyCode === counterCurrencyCode) {
      errors.push('No need to exchange, this is the same balance!');
    }

    // if base amount is greater than the base balance, create an error
    if (baseAmount > balances[baseCurrencyCode]) {
      errors.push('Oh no! You do not have the funds to make this exchange.');
    }

    if (errors.length === 0) {
      // debit base amount from base balance
      data.baseBalance -= parseFloat(baseAmount);
      // credit counter balance with counter amount
      data.counterBalance += parseFloat(counterAmount);
    } else {
      alert(errors[0]);
    }

    return {
      type: UPDATE_BALANCES,
      payload: {
        errors,
        data
      }
    };

}

