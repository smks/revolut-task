import currencyCodes from './currency-codes';

const symbols = {};
symbols[currencyCodes.GBP] = 650;
symbols[currencyCodes.EUR] = 0;
symbols[currencyCodes.USD] = 125;
export default symbols;