import currencyCodes from './currency-codes';

const symbols = {};
symbols[currencyCodes.GBP] = '£';
symbols[currencyCodes.EUR] = '€';
symbols[currencyCodes.USD] = '$';
export default symbols;