import { h, Component } from 'preact';
import accounting from 'accounting';
import currencySymbols from './../../constants/currency-symbols';
import duration from './../../constants/duration';
import style from './style.scss';

export default class CurrencyBar extends Component {

  constructor(props) {
    super(props);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.onChangeCurrencyCode = this.onChangeCurrencyCode.bind(this);
  }

  getCurrencyCodeOptions() {
    const { currencyCodes } = this.props;
    if (!currencyCodes) {
      return null;
    }
    return this.props.currencyCodes
      .map(code => <option value={code} selected={code === this.props.currencyCode ? 'selected' : ''}>{code}</option>);
  }

  handleFocus(event) {
    event.target.select();
  }

  handleKeyUp(event) {

    if (this.keyUpTimer) {
      clearInterval(this.keyUpTimer);
    }

    let amount = event.target.value;

    if (!amount.match(/[0-9]*\.?[0-9]*/)) {
      amount = amount.replace(/^[A-Za-z]/, '');
    }

    if (isNaN(amount)) {
      return false;
    }

    if (amount === '') {
      amount = '1';
    }

    this.keyUpTimer = setTimeout(
      this.props.onAmountChange.bind(null, parseFloat(amount)),
      duration.KEY_UP_DELAY
    );
  }

  onChangeCurrencyCode(e) {
    this.props.changeCurrencyCode(e.target.value);
  }

  render() {
    const { amount, currencyCode, balance } = this.props;
    const currencyCodeOptions = this.getCurrencyCodeOptions();

    return (
      <div class={style['currency-bar']}>
        <div class={style['currency-codes']}>
				  <select class={style['currency-codes']} onChange={this.onChangeCurrencyCode}>
            {currencyCodeOptions}
          </select>
        </div>
        <div class={style['amount']}>
          <input type="text" value={amount} maxLength="6" onFocus={this.handleFocus} onKeyUp={this.handleKeyUp}/>
        </div>
        <p class={style.balance}><strong>Balance:</strong> {accounting.formatMoney(balance, { symbol: currencySymbols[currencyCode] })}</p>
      </div>
    );
  }
}
