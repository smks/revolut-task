import { h, Component } from 'preact';
import style from './style.scss';

export default class Card extends Component {
  render() {
    return (
      <div class={style.card}>
        {this.props.children}
      </div>
    );
  }
}
