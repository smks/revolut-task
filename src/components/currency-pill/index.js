import { h, Component } from 'preact';
import accounting from 'accounting';
import currencySymbols from './../../constants/currency-symbols';
import style from './style.scss';

export default class CurrencyBar extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { baseCurrencyCode, counterCurrencyCode, baseRate, counterRate } = this.props;
    return (
      <div class={style['currency-pill']}>
        <p>{accounting.formatMoney(baseRate, { symbol: currencySymbols[baseCurrencyCode] })} = {accounting.formatMoney(counterRate, { symbol: currencySymbols[counterCurrencyCode] })}</p>
      </div>
    );
  }
}
