import { h, Component } from 'preact';
import style from './style.scss';
import Card from './../../components/card';
import { Link } from 'preact-router';
import { connect } from 'preact-redux';

class Home extends Component {
  render() {
    return (
      <div class={style.home}>
        <Card>
          <h1>Welcome, {this.props.name}</h1>
          <p>This is a task carried out by <strong>Shaun Michael Stone</strong> using Preact, Redux, Sass, Webpack. I chose Preact because it's a lightweight equivalent to React, Redux as the favoured state container and Sass due to preference.</p>
          <p>To view the task, click <strong>'Exchange Money'</strong> to get started.</p>
          <Link class={style.button} href="/exchange">Exchange Money</Link>
        </Card>
        <Card>
          <p><small>I decided not to use the Open Exchange Rates API, as it does not allow you to change the base rate, and the base rate is forced as USD. So I opted for <a href="https://github.com/hakanensari/fixer-io">fixer-io</a> because it's 100% free, and allows me to choose the base rate, as well as the few select symbols/currency codes I needed for this task, this lightens the payload.</small></p>
          <p>Shaun Stone</p>
        </Card>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    name: state.customerDetails.name
  };
}

export default connect(mapStateToProps)(Home);