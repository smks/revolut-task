import { h, Component } from 'preact';
import CurrencyBar from './../../components/currency-bar';
import CurrencyPill from './../../components/currency-pill';
import currencyTypes from '../../constants/currency-types';
import duration from '../../constants/duration';
import style from './style.scss';
import { connect } from 'react-redux';
import Card from './../../components/card';
import { Link } from 'preact-router';
import {
  fetchExchangeRates,
  updateBaseCurrencyCode,
  updateCounterCurrencyCode,
  updateBaseAmount,
  updateCounterAmount,
  updateBalances
} from '../../actions/index';

class Exchange extends Component {

  constructor(props) {
    super(props);
    this.getRates = this.getRates.bind(this);
    this.onBaseCurrencyCodeChange = this.onBaseCurrencyCodeChange.bind(this);
    this.onCounterCurrencyCodeChange = this.onCounterCurrencyCodeChange.bind(this);
    this.onBaseAmountChange = this.onBaseAmountChange.bind(this);
    this.onCounterAmountChange = this.onCounterAmountChange.bind(this);
    this.onClickExchange = this.onClickExchange.bind(this);
  }

  componentWillMount() {
    // call get rates as soon as we switch to this screen, and then every ten seconds after that
    this.getRates();
    this.rateFetchInterval = setInterval(this.getRates, duration.FETCH_RATES_DURATION);
  }

  componentWillUnmount() {
    clearTimeout(this.rateFetchInterval);
  }

  getRates() {
    this.props.fetchExchangeRates();
  }

  onBaseCurrencyCodeChange(code) {
    this.props.updateBaseCurrencyCode(code);
  }

  onCounterCurrencyCodeChange(code) {
    this.props.updateCounterCurrencyCode(code);
  }

  onBaseAmountChange(amount) {
    this.props.updateBaseAmount(amount);
  }

  onCounterAmountChange(amount) {
    this.props.updateCounterAmount(amount);
  }

  onClickExchange() {
    const { amounts, balances } = this.props;
    this.props.updateBalances({ ...amounts, balances });
  }

  render() {
    const { balances, amounts, currencyCodes } = this.props;
    const { baseAmount, baseCurrencyCode, counterAmount, counterCurrencyCode, baseRate, counterRate } = amounts;
    
    return (
      <div class={style.exchange}>
        <div class={style['currency-container']}>
          <CurrencyBar
            amount={baseAmount}
            currencyCode={baseCurrencyCode}
            currencyCodes={currencyCodes}
            balance={balances[baseCurrencyCode]}
            changeCurrencyCode={this.onBaseCurrencyCodeChange}
            onAmountChange={this.onBaseAmountChange}
            type={currencyTypes.BASE}
          />
          <CurrencyPill
            baseCurrencyCode={baseCurrencyCode}
            counterCurrencyCode={counterCurrencyCode}
            baseRate={baseRate}
            counterRate={counterRate}
          />
          <CurrencyBar
            class={style['second-currency-bar']}
            amount={counterAmount}
            currencyCode={counterCurrencyCode}
            currencyCodes={currencyCodes}
            balance={balances[counterCurrencyCode]}
            changeCurrencyCode={this.onCounterCurrencyCodeChange}
            onAmountChange={this.onCounterAmountChange}
            type={currencyTypes.COUNTER}
          />
        </div>
        <Card>
          <Link class={style.button} onClick={this.onClickExchange}>Exchange Money</Link>
        </Card>
      </div>
    );
  }
}

function mapStateToProps({ customerDetails, amounts, currencyCodes }) {
  return {
    balances: customerDetails.balances,
    amounts,
    currencyCodes
  };
}

export default connect(mapStateToProps, {
  fetchExchangeRates,
  updateBaseCurrencyCode,
  updateBaseAmount,
  updateCounterCurrencyCode,
  updateCounterAmount,
  updateBalances
})(Exchange);